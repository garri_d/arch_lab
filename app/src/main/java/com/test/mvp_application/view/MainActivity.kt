package com.test.mvp_application.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.test.mvp_application.R
import com.test.mvp_application.presenter.Presenter
import com.test.mvp_application.repository.database.entity.Client
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.alert_dialog.view.*
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity(), IView {

    lateinit var presenter : Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = Presenter(this,this)

    }

    override fun insert() {

    }

    fun onClick(view: View){
        when(view.id){
            R.id.floatBtn -> {

                val allertView = this.layoutInflater.inflate(R.layout.alert_dialog,null)

                AlertDialog.Builder(this).apply {
                    title = "Add client"
                    setView(allertView)
                    setPositiveButton("Add"){ dialog, _ ->
                        val client = Client(
                            allertView.et1.text.toString().toInt(),
                            allertView.et2.text.toString().toLong(),
                            allertView.et3.text.toString(),
                            allertView.et4.text.toString(),
                            allertView.et5.text.toString(),
                            allertView.et6.text.toString()
                        )

                        CoroutineScope(Dispatchers.Main).launch {
                               presenter.addClient(client)
                            Log.d("CHECKOWN","Main_activity_corout_scope")

                        }

                        dialog.dismiss()
                    }
                }.create().show()
            }
            R.id.button -> {
                CoroutineScope(Dispatchers.Main).launch {
                    presenter.addClient(Client(53,23123,"nazi1938", "Aldolf","Mozart", "Alex"))
                }
            }
            R.id.button2 -> {
                CoroutineScope(Dispatchers.Main).launch {
                    presenter.getClients()
                }
            }

        }

    }

    override fun showInTextView(string: String) {
        textView.text = string
    }


    override suspend fun show(list: List<Client>) {
            textView.text = list.map { it.firstName}.joinToString { "\n" }
    }

}
