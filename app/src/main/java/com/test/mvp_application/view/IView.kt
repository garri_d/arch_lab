package com.test.mvp_application.view

import com.test.mvp_application.repository.database.entity.Client

interface IView {
    fun insert()
    suspend fun show(list: List<Client>)
    fun showInTextView(string: String)
}