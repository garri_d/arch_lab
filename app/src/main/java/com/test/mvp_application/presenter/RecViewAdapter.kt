package com.test.mvp_application.presenter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.test.mvp_application.R
import com.test.mvp_application.repository.database.entity.Client
import kotlinx.android.synthetic.main.viewhodler.view.*

class RecViewAdapter(var list: MutableList<Client>): RecyclerView.Adapter<RecViewAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.viewhodler, parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
         return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = list[position].toString()
    }

    fun add(client: Client){
        list.add(client)
        notifyItemInserted(list.size - 1 )
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val textView  = itemView.findViewById<TextView>(R.id.textView)
    }
}