package com.test.mvp_application.presenter

import android.content.Context
import android.util.Log
import com.test.mvp_application.repository.RepositoryInterface
import com.test.mvp_application.repository.database.entity.Client
import com.test.mvp_application.view.IView
import com.test.mvp_application.view.MainActivity
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import java.lang.Exception

class Presenter(val view: IView, val context: Context): IPresenter, KodeinAware{

    override val kodein by closestKodein(context)

    val repos: RepositoryInterface by instance()


    suspend fun addClient(client: Client){
        Log.d("CHECKOWN","presener, add client, beffore calling client services")
        try {
            repos.insertClient(client)
        }
        catch (e: Exception) {
            view.showInTextView(e.message!!)
        }
    }

    suspend fun getClients() {
        try {
            val list = repos.getClients()
            view.show(list)
        }
        catch (e: Exception){
            view.showInTextView(e.message!!)
        }

    }

    suspend fun deleteDisk() {
            repos.diskService.deleteDisk(1)
    }


}