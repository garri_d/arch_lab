package com.test.mvp_application

import android.app.Application
import com.test.mvp_application.presenter.IPresenter
import com.test.mvp_application.presenter.Presenter
import com.test.mvp_application.repository.AppRepository
import com.test.mvp_application.repository.RepositoryInterface
import com.test.mvp_application.repository.database.AppDataBase
import com.test.mvp_application.repository.database.DataBaseInterface
import com.test.mvp_application.repository.database.dao.ClientDao
import com.test.mvp_application.repository.database.dao.VinylDiskDao
import com.test.mvp_application.repository.services.ClientService
import com.test.mvp_application.repository.services.DiskService
import com.test.mvp_application.view.IView
import com.test.mvp_application.view.MainActivity
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton


//acting like something changed

class MyApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        //comment
        import(androidXModule(this@MyApplication))
        bind<DataBaseInterface>() with singleton { AppDataBase(instance()) }
        bind<ClientDao>() with singleton { instance<DataBaseInterface>().clientDao() }
        bind<VinylDiskDao>() with singleton { instance<DataBaseInterface>().vinylDiskDao() }
        bind<ClientService>() with provider { ClientService(instance<DataBaseInterface>().clientDao() ) }
        bind<DiskService>() with provider { DiskService(instance<DataBaseInterface>().vinylDiskDao()) }
        bind<RepositoryInterface>() with singleton { AppRepository(instance(),instance(),instance()) }
        bind<IPresenter>() with singleton { Presenter(instance(),instance())}
        //comment, are you serious???? 123
    }

}