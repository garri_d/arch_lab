package com.test.mvp_application.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.test.mvp_application.repository.database.dao.*
import com.test.mvp_application.repository.database.entity.*
import kotlinx.coroutines.selects.SelectInstance


@Database(
    entities = [
        Order::class,
        Seller::class,
        Client::class,
        VinylDisk::class,
        Order_vinylDisk::class
    ],
    version = 3,
    exportSchema = false
)
abstract class AppDataBase (): RoomDatabase(), DataBaseInterface {

    abstract override fun orderDao(): OrderDao
    abstract override fun clientDao(): ClientDao
    abstract override fun sellerDao(): SellerDao
    abstract override fun vinylDiskDao(): VinylDiskDao
    abstract override fun order_vinylDiskDao() : Order_vinylDiskDao

    companion object{

        @Volatile
        private var instance: AppDataBase? =  null

        private val LOCK = Any()
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,AppDataBase::class.java,"DataBase").build()
    }

}
