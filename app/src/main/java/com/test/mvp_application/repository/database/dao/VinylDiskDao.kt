package com.test.mvp_application.repository.database.dao


import androidx.room.*
import com.test.mvp_application.repository.database.entity.VinylDisk

@Dao
interface VinylDiskDao {
    @Insert
    fun insert(vinylDisk: VinylDisk)

    @Update
    fun update(vinylDisk: VinylDisk)

    @Delete
    fun delete(vinylDisk: VinylDisk)

    @Query("SELECT * FROM VinylDisk")
    fun getAllDisks(): List<VinylDisk>

    @Query("SELECT * FROM VinylDisk WHERE VinylDisk.id = :id")
    fun getVinylDiksById(id: Int): List<VinylDisk>

    @Query("Delete FROM VinylDisk WHERE VinylDisk.id = :id")
    fun deleteById(id: Int)
}

