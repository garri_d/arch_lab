package com.test.mvp_application.repository.database.dao

import androidx.room.*
import com.test.mvp_application.repository.database.entity.Client

@Dao
interface ClientDao {

    @Insert
    suspend fun insert(client: Client)

    @Update
    fun update(client: Client)

    @Delete
    fun delete(client: Client)

    @Query("SELECT * FROM Client")
    suspend fun getAllClients(): List<Client>

    @Query("SELECT * FROM Client WHERE Client.id = :id")
    fun getClientById(id: Int): List<Client>
}