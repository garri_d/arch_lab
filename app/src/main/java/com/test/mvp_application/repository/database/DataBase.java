package com.test.mvp_application.repository.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.test.mvp_application.repository.database.dao.ClientDao;
import com.test.mvp_application.repository.database.dao.OrderDao;
import com.test.mvp_application.repository.database.dao.Order_vinylDiskDao;
import com.test.mvp_application.repository.database.dao.SellerDao;
import com.test.mvp_application.repository.database.dao.VinylDiskDao;
import com.test.mvp_application.repository.database.entity.Order;
import com.test.mvp_application.repository.database.entity.Seller;
import com.test.mvp_application.repository.database.entity.Order;
import com.test.mvp_application.repository.database.entity.Order_vinylDisk;
import com.test.mvp_application.repository.database.entity.Client;
import com.test.mvp_application.repository.database.entity.VinylDisk;

@Database(
        entities = {
                Order.class,
                Seller.class,
                Client.class,
                VinylDisk.class,
                Order_vinylDisk.class
        },
        version = 2,
        exportSchema = false
)
public abstract class DataBase extends RoomDatabase {

    static private DataBase instance = null;

    public abstract OrderDao orderDao();
    public abstract ClientDao clientDao();
    public abstract SellerDao sellerDao();
    public abstract VinylDiskDao vinylDiskDao();
    public abstract Order_vinylDiskDao order_vinylDiskDao();

    public static synchronized DataBase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),DataBase.class,"DataBase").
            fallbackToDestructiveMigration().build();
        }
        return instance;
    }

}

