package com.test.mvp_application.repository.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Client(
    @PrimaryKey
    val id: Int,
    var phoneNubmer: Long,
    val email: String,
    val firstName: String,
    val lastName: String,
    val fatherName: String
)
