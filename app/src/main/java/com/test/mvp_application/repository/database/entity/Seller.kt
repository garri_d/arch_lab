package com.test.mvp_application.repository.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity
data class Seller (
    @PrimaryKey
    val id: Int,
    val firstName: String,
    val lastName: String,
    val fatherName: String,
    val phoneNumber: Long
)

