package com.test.mvp_application.repository.database.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*


/*    indices = arrayOf(Index(value = ["client_id","seller_id"],unique = true)),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Client::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("client_id"),
            onDelete = CASCADE
        ),
        ForeignKey(
            entity = Seller::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("seller_id"),
            onDelete = CASCADE
        )
    )*/

@Entity
data class Order(
    @PrimaryKey
    val id: Int,
    val vinylDisk_id: Int,
    val client_id: Int,
    val disk_id: Int,
    val payment_method: String,
    val seller_id: Int
)
