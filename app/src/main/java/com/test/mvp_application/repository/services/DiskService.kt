package com.test.mvp_application.repository.services

import com.test.mvp_application.repository.database.dao.VinylDiskDao
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class DiskService(val vinylDiskDao: VinylDiskDao) {

    suspend fun deleteDisk(id: Int){
        val diskBand = vinylDiskDao.getVinylDiksById(id).firstOrNull()?.bandName
        //проверяем, может быть
        // в бд нет никаких дисков
        diskBand.let {
            vinylDiskDao.deleteById(id)
        }

        withContext(IO){
            if (!vinylDiskDao.getAllDisks().map { it.bandName }.contains(diskBand)){
                //RETROFIT
                //SERVER REQUEST
                // LET'S SAY TO THE RADIO
                // THAT DISKs WITH THIS BAND GONE
                println("RADIO: DISKS WITH THIS BAND ARE GONE")
            }
        }

    }
}