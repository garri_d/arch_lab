package com.test.mvp_application.repository

import android.content.Context
import com.test.mvp_application.repository.database.AppDataBase
import com.test.mvp_application.repository.database.DataBaseInterface
import com.test.mvp_application.repository.database.entity.Client
import com.test.mvp_application.repository.services.ClientService
import com.test.mvp_application.repository.services.DiskService
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance


class AppRepository (
    val context: Context,
    override val clientService: ClientService,
    override val diskService: DiskService): RepositoryInterface, KodeinAware {

    override val kodein by closestKodein(context)

     private val dataBase: DataBaseInterface by instance()

    override suspend fun insertClient(client: Client){
            clientService.insertClient(client)
    }

    override suspend fun getClients(): List<Client> {
        return clientService.getAllClients()
    }

}
