package com.test.mvp_application.repository.database.dao

import androidx.room.*
import com.test.mvp_application.repository.database.entity.Order_vinylDisk

@Dao
interface Order_vinylDiskDao {

    @Insert
    fun insert(note: Order_vinylDisk)

    @Update
    fun update(note: Order_vinylDisk)

    @Delete
    fun delete(note: Order_vinylDisk)

    @Query("SELECT * FROM Order_vinylDisk")
    fun getAllOrder_vinylDisk(): List<Order_vinylDisk>


}
