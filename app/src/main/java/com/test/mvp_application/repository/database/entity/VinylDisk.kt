package com.test.mvp_application.repository.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class VinylDisk (
    @PrimaryKey
    val id: Int,
    val bandName: String,
    val album: String,
    val year: Int,
    val qty: Int,
    val price: Int
)
