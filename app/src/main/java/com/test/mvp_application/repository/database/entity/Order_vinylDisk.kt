package com.test.mvp_application.repository.database.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

/*  foreignKeys = arrayOf(
        ForeignKey(
            entity = VinylDisk::class,
            parentColumns = arrayOf("vinylDiskId"),
            childColumns = arrayOf("id"),
            onDelete = CASCADE
        ),
        ForeignKey(
            entity = Order::class,
            parentColumns = arrayOf("orderId"),
            childColumns = arrayOf("id"),
            onDelete = CASCADE
        )
    )*/

@Entity
data class Order_vinylDisk(
    @PrimaryKey
    val orderId: Int,
    val vinylDiskId: Int
)

