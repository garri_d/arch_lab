package com.test.mvp_application.repository

import com.test.mvp_application.repository.database.entity.Client
import com.test.mvp_application.repository.services.ClientService
import com.test.mvp_application.repository.services.DiskService

interface RepositoryInterface {
    val clientService: ClientService
    val diskService: DiskService
    suspend fun insertClient(client: Client)
    suspend fun getClients(): List<Client>
}