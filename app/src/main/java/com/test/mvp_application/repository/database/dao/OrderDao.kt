package com.test.mvp_application.repository.database.dao

import androidx.room.*
import com.test.mvp_application.repository.database.entity.Order

@Dao
interface OrderDao {
    @Insert
    fun insert(order: Order)

    @Update
    fun update(order: Order)

    @Delete
    fun delete(order: Order)

    @Query("SELECT * FROM `Order`")
    fun getAllOrders(): List<`Order`>

    @Query("SELECT * FROM `Order`WHERE `Order`.id = :id")
    fun getOrderById(id: Int): List<`Order`>
}

