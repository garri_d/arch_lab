package com.test.mvp_application.repository.database.dao

import androidx.room.*
import com.test.mvp_application.repository.database.entity.Order
import com.test.mvp_application.repository.database.entity.Seller

@Dao
interface SellerDao {
    @Insert
    fun insert(seller: Seller)

    @Update
    fun update(seller: Seller)

    @Delete
    fun delete(seller: Seller)

    @Query("SELECT * FROM Seller")
    fun getAllSellers(): List<Seller>

    @Query("SELECT * FROM Seller WHERE Seller.id = :id")
    fun getSellerById(id: Int): List<Seller>
}

