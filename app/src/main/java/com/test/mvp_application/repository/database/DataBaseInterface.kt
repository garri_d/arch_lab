package com.test.mvp_application.repository.database

import com.test.mvp_application.repository.database.dao.*

interface DataBaseInterface {
    abstract fun orderDao(): OrderDao
    abstract fun clientDao(): ClientDao
    abstract fun sellerDao(): SellerDao
    abstract fun vinylDiskDao(): VinylDiskDao
    abstract fun order_vinylDiskDao() : Order_vinylDiskDao

}